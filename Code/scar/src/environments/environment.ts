// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // Your web app's Firebase configuration
  firebase: {
    apiKey: 'AIzaSyAnuAUAWFUMfaqm-EVq-EjHVhexQYAaGuE',
    authDomain: 'sca-risco.firebaseapp.com',
   databaseURL: 'https://sca-risco.firebaseio.com',
    projectId: 'sca-risco',
    storageBucket: 'sca-risco.appspot.com',
    messagingSenderId: '1039715274770',
    appId: '1:1039715274770:web:827e864d169450af4cc8cb',
    measurementId: 'G-9WX416WV29'
  }
  // Initialize Firebase
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as 'zone.run`, `zoneDelegate.invokeTask`.
 *
 *'This import should be commented out in production mode because it will have a negative impa't
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
