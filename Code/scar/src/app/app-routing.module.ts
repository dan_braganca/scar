import { EditEventoComponent } from './empresa/analise/edit-evento/edit-evento.component';
import { AnaliseComponent } from './empresa/analise/analise.component';
import { EditProcessoComponent } from './empresa/processos/edit/edit.component';
import { AuthGuard } from './guards/auth.guard';
import { ProcessosComponent } from './empresa/processos/processos.component';
import { DetalheComponent } from './empresa/detalhe/detalhe.component';
import {
  redirectUnauthorizedTo,
  AngularFireAuthGuard,
  canActivate,
} from '@angular/fire/auth-guard';
import { EmpresaComponent } from './empresa/empresa.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './admin/login/login.component';
import { RegisterComponent } from './admin/register/register.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { EditEmpresaComponent } from './empresa/edit/edit.component';
import { ControlesComponent } from './empresa/analise/controles/controles.component';
import { RespostaRiscoComponent } from './empresa/analise/resposta-risco/resposta-risco.component';
import { AvaliacaoRiscoComponent } from './empresa/analise/avaliacao-risco/avaliacao-risco.component';

const redirectUnauthorizedToLogin = redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'login',
    /* loadChildren: 'app/admin.module#AdminModule' */ component: LoginComponent,
  },
  { path: 'admin', /* canActivate: [AuthGuard], */ component: AdminComponent },
  { path: 'empresa', component: EmpresaComponent },
  { path: 'empresa/edit/:keyEmpresa', component: EditEmpresaComponent },
  /* { path: '**', component: LoginComponent }, */
  { path: 'empresa/detalhe/:key', component: DetalheComponent },
  { path: 'empresa/:key/processos', component: ProcessosComponent },
  {
    path: 'empresa/:keyEmpresa/processos/:keyProcesso',
    component: EditProcessoComponent,
  },
  { path: 'empresa/:keyEmpresa/analise', component: AnaliseComponent },
  {
    path: 'empresa/:keyEmpresa/analise/:keyProcesso/:keyEvento/edit',
    component: EditEventoComponent,
  },
  {
    path: 'empresa/:keyEmpresa/analise/:keyProcesso/:keyEvento/controles',
    component: ControlesComponent,
  },
  {
    path: 'empresa/:keyEmpresa/analise/:keyProcesso/:keyEvento/resposta-risco',
    component: RespostaRiscoComponent,
  },
  {
    path: 'empresa/:keyEmpresa/analise/:keyProcesso/:keyEvento/avaliacao-risco',
    component: AvaliacaoRiscoComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
