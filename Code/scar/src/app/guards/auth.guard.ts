import { AuthService } from './../services/auth.service';
import { UserService } from './../services/user.service';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    public afAuth: AngularFireAuth,
    public userService: UserService,
    private router: Router,
    private authService: AuthService
  ) {}

  // CANACTIVATE AS A PROMISE
  // canActivate(): Promise<boolean> {
  //   return new Promise((resolve, reject) => {
  //     this.userService.getCurrentUser().then(
  //       (user) => {
  //         this.router.navigate(['']);
  //         return resolve(false);
  //       },
  //       (err) => {
  //         return resolve(true);
  //       }
  //     );
  //   });
  // }

  // CANACTIVATE AS A BOOLEAN
  canActivate() {
    if (this.authService.isLoggedIn()){
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }

  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean>{
  //   return this.authService.isLoggedIn().pipe(
  //     tap((logged) => {
  //       if (!logged) {
  //         this.router.navigateByUrl('login');
  //       }
  //     })
  //   );
  // }
}
