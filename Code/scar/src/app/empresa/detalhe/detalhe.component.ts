import { Location } from '@angular/common';
import { EmpresaService } from './../../services/empresa.service';
import { EmpresaModel } from './../../models/empresa.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProcessoModel } from 'src/app/models/processo.model';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.scss']
})
export class DetalheComponent implements OnInit {

  empresa: EmpresaModel = new EmpresaModel();

  processos: ProcessoModel[];

  columnsToDisplay = [
    'nome_processo',
    'descricao_processo',
    'editar'
  ];

  constructor(
    private empresaService: EmpresaService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {  }

  ngOnInit(): void {
    const key: string = this.route.snapshot.paramMap.get('key');
    this.empresaService.getEmpresa(key).subscribe(
      (empresa) => {
        this.getProcessos(empresa);
        this.empresa = empresa;
      },
      (error) => console.log('Erro em detalhe.component: ', error)
    );

    console.log(this.empresa.processos);
  }

  getProcessos(empresa: EmpresaModel): void {
    this.empresaService.getProcessosEmpresa(empresa).subscribe((array) => {
      empresa.processos = array;
    });
  }

  backURL(){
    this.location.back();
  }
}
