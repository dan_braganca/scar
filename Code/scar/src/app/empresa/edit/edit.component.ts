import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { EmpresaModel } from 'src/app/models/empresa.model';
import { EmpresaService } from 'src/app/services/empresa.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditEmpresaComponent implements OnInit {
  public empresa: EmpresaModel;

  constructor(
    private empresaService: EmpresaService,
    private location: Location,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    if (keyEmpresa !== 'novo') {
      this.empresaService.getEmpresa(keyEmpresa).subscribe(
        (empresa) => {
          this.empresa = empresa;
        },
        (error) => console.log('Erro em EmpresaEdit.component: ', error)
      );
    } else {
      this.empresa = new EmpresaModel();
    }
  }

  salvar(): void {
    if (this.empresa.key == null) {
      this.empresaService.addEmpresa(this.empresa);
    } else {
      this.empresaService.setEmpresa(this.empresa);
    }
    this.backURL();
  }

  async deleteEmpresa() {
    await Swal.fire({
      title: 'Tem certeza de que deseja excluir esta empresa?',
      text: 'Esta ação é irreversível',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.empresaService.deleteEmpresa(this.empresa);
        this.backURL();
      }
    });
  }

  backURL(): void {
    this.location.back();
  }
}
