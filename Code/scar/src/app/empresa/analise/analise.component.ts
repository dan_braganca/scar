import {
  GoogleChartComponent,
  GoogleChartsModule,
  ScriptLoaderService,
} from 'angular-google-charts';
import { AnaliseService } from './../../services/analise.service';
import { Location } from '@angular/common';
import { EmpresaService } from './../../services/empresa.service';
import { EmpresaModel } from './../../models/empresa.model';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  NgZone,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventoModel } from 'src/app/models/evento.model';
import { customData } from 'src/app/models/evento_customData.model';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { ProcessoModel } from 'src/app/models/processo.model';
import { MatTable } from '@angular/material/table';

// declare var google: any;

@Component({
  selector: 'app-analise',
  templateUrl: './analise.component.html',
  styleUrls: ['./analise.component.scss'],
})
export class AnaliseComponent implements OnInit, AfterViewInit {
  constructor(
    private empresaService: EmpresaService,
    private route: ActivatedRoute,
    private location: Location,
    private snackBar: MatSnackBar,
    private analiseService: AnaliseService,
    private loaderService: ScriptLoaderService
  ) {
    this.empresa = new EmpresaModel();
    this.source = [];
    this.todosEventos = [];
  }

  empresa: EmpresaModel;

  source: customData[];

  todosEventos: EventoModel[];

  @ViewChild(MatTable) table: MatTable<any>;

  columnsToDisplay = [
    'nome_processo',
    'descricao_evento',
    'categoria',
    'impacto',
    'probabilidade',
    'risco_inerente',
    'avaliacao_risco',
    'controles',
    'resposta_nr',
    'edit',
  ];

  /////////////////////////////////////////////////////
  //               DADOS DA MATRIZ                   //

  dataSet: any[] = [];

  chart: any;

  chartTitle = 'Matriz de Risco';
  chartType = 'ScatterChart';
  chartData: any[] = [
    // [1, 1]
    //this.dataSet
  ];
  columnNames = ['impacto', 'probabilidade'];
  chartOptions = {
    hAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 5
      },
      title: 'Impacto',
      minValue: 0,
      maxValue: 5
    },
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 5
      },
      title: 'Probabilidade',
      minValue: 0,
      maxValue: 5
    },
    legend: 'none',
  };
  chartWidth = 500;
  chartHeight = 500;

  //                                                 //
  /////////////////////////////////////////////////////

  async ngOnInit() {
    await this.empresaService
      .getEmpresa(this.route.snapshot.paramMap.get('keyEmpresa'))
      .subscribe(
        async (empresa) => {
          await this.empresaService
            .getProcessosEmpresa(empresa)
            .subscribe((array) => {
              array.forEach(async (processo) => {
                await this.analiseService
                  .getEventosByProcesso(this.empresa.key, processo.key)
                  .subscribe((eventos) => {
                    eventos.forEach((e) => {
                      let sourceObject: customData = {
                        nome_processo: processo.nome_processo,
                        key_processo: processo.key,
                        key_evento: e.key,
                        key_empresa: this.empresa.key,
                        descricao_evento: e.descricao,
                        categoria: e.categoria,
                        probabilidade: e.probabilidade,
                        impacto: this.analiseService.calculoImpacto(e),
                        risco_inerente: this.analiseService.calculoRisco(
                          e
                        ),
                      };
                      this.todosEventos.push(e);
                      if (typeof e.impacto === 'string' && typeof e.probabilidade === 'string'){
                        this.dataSet.push([parseInt(e.impacto), parseInt(e.probabilidade)]);
                      } else if (typeof e.impacto === 'string' && typeof e.probabilidade === 'number'){
                        this.dataSet.push([parseInt(e.impacto), e.probabilidade]);
                      } else if (typeof e.probabilidade === 'string' && typeof e.impacto === 'number'){
                        this.dataSet.push([e.impacto, parseInt(e.probabilidade)]);
                      } else {
                        this.dataSet.push([e.impacto, e.probabilidade]);
                      }
                      console.log(this.dataSet);
                      this.source.push(sourceObject);
                      this.table.renderRows();
                    });
                    this.chartData = this.dataSet;
                  });
                empresa.processos = array;
                this.empresa.processos = array;
              });
            });
          this.empresa = empresa;
        },
        (error) => console.log('Erro em analise.component: ', error)
      );
  }

  ngAfterViewInit() {}

  backURL(): void {
    this.location.back();
  }

  // updateChartData(newData: Array<Array<string | int>>) {
  //   this.chartData = newData;
  // }
}
