import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpresaModel } from 'src/app/models/empresa.model';
import { EventoModel } from 'src/app/models/evento.model';
import { ProcessoModel } from 'src/app/models/processo.model';
import { AnaliseService } from 'src/app/services/analise.service';
import { EmpresaService } from 'src/app/services/empresa.service';

@Component({
  selector: 'app-controles',
  templateUrl: './controles.component.html',
  styleUrls: ['./controles.component.scss'],
})
export class ControlesComponent implements OnInit {
  keyEmpresa = '';
  keyEvento = '';
  keyProcesso = '';

  processo: ProcessoModel;

  empresa: EmpresaModel;

  evento: EventoModel;

  selectProcessosList: ProcessoModel[];

  constructor(
    private route: ActivatedRoute,
    private empresaService: EmpresaService,
    private analiseService: AnaliseService,
    private location: Location
  ) {}

  async ngOnInit(): Promise<void> {
    this.keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    this.keyEvento = this.route.snapshot.paramMap.get('keyEvento');
    this.keyProcesso = this.route.snapshot.paramMap.get('keyProcesso');

    await this.empresaService
      .getEmpresa(this.keyEmpresa)
      .subscribe((empresa) => {
        this.empresa = empresa;
      });

    await this.empresaService
      .getProcesso(this.keyEmpresa, this.keyProcesso)
      .subscribe(
        (processo) => {
          this.processo = processo;
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
    await this.analiseService
      .getEvento(this.keyEmpresa, this.keyProcesso, this.keyEvento)
      .subscribe(
        (evento) => {
          this.evento = evento;
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
  }

  salvar(): void {
    this.evento.risco_residual = this.analiseService.calculoRiscoResidual(this.evento);
    console.log(this.evento);
    this.analiseService.setEvento(
      this.keyEmpresa,
      this.evento.key_processo,
      this.evento
    );
    this.backURL();
  }

  backURL(): void {
    this.location.back();
  }
}
