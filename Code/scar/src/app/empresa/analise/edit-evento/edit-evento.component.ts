import { EmpresaModel } from 'src/app/models/empresa.model';
import { EventoModel } from 'src/app/models/evento.model';
import { ProcessoModel } from 'src/app/models/processo.model';
import { formatDate, Location } from '@angular/common';
import { AnaliseService } from './../../../services/analise.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpresaService } from 'src/app/services/empresa.service';
import swal from 'sweetalert2';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-evento',
  templateUrl: './edit-evento.component.html',
  styleUrls: ['./edit-evento.component.scss'],
})
export class EditEventoComponent implements OnInit {
  keyEmpresa = '';
  keyProcesso = '';
  keyEvento = '';

  processo: ProcessoModel;

  empresa: EmpresaModel;

  evento: EventoModel;

  selectProcessosList: ProcessoModel[];

  constructor(
    private route: ActivatedRoute,
    private empresaService: EmpresaService,
    private analiseService: AnaliseService,
    private location: Location
  ) {}

  async ngOnInit(): Promise<void> {
    this.keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    this.keyEvento = this.route.snapshot.paramMap.get('keyEvento');
    this.keyProcesso = this.route.snapshot.paramMap.get('keyProcesso');

    await this.empresaService
      .getEmpresa(this.keyEmpresa)
      .subscribe((empresa) => {
        this.empresa = empresa;
        this.getSelectProcessos();
      });

    if (this.keyEvento !== 'novoEvento' || this.keyEvento == null) {
      await this.empresaService
        .getProcesso(this.keyEmpresa, this.keyProcesso)
        .subscribe(
          (processo) => {
            this.processo = processo;
          },
          (error) => console.log('Erro em EditEvento.component: ', error)
        );
      await this.analiseService
        .getEvento(this.keyEmpresa, this.keyProcesso, this.keyEvento)
        .subscribe(
          (evento) => {
            this.evento = evento;
          },
          (error) => console.log('Erro em EditEvento.component: ', error)
        );
    } else {
      this.evento = new EventoModel();
    }
  }

  getSelectProcessos(): void {
    this.empresaService
      .getProcessosEmpresa(this.empresa)
      .subscribe((changes) => {
        this.selectProcessosList = changes;
      });
  }

  salvar(): void {
    if (this.evento.key == null) {
      this.analiseService.addEvento(
        this.keyEmpresa,
        this.evento.key_processo,
        this.evento
      );
    } else {
      this.analiseService.setEvento(
        this.keyEmpresa,
        this.evento.key_processo,
        this.evento
      );
    }
    this.backURL();
  }

  backURL(): void {
    this.location.back();
  }

  async deleteEvento(): Promise<void> {
    await Swal.fire({
      title: 'Tem certeza de que deseja excluir este evento?',
      text: 'Esta ação é irreversível',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.analiseService.deleteEvento(
          this.keyEmpresa,
          this.keyProcesso,
          this.evento
        );
        this.backURL();
      }
    });
  }
}
