import { formatDate, Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpresaModel } from 'src/app/models/empresa.model';
import { EventoModel } from 'src/app/models/evento.model';
import { ProcessoModel } from 'src/app/models/processo.model';
import { TextosInterfaceModel } from 'src/app/models/textos_interface.model';
import { AnaliseService } from 'src/app/services/analise.service';
import { EmpresaService } from 'src/app/services/empresa.service';

@Component({
  selector: 'app-avaliacao-risco',
  templateUrl: './avaliacao-risco.component.html',
  styleUrls: ['./avaliacao-risco.component.scss'],
})
export class AvaliacaoRiscoComponent implements OnInit {
  keyEmpresa = '';
  keyEvento = '';
  keyProcesso = '';

  startDateString: string;
  endDateString: string;

  processo: ProcessoModel;

  empresa: EmpresaModel;

  evento: EventoModel;

  textos: TextosInterfaceModel;

  textoHtml = {};

  constructor(
    private route: ActivatedRoute,
    private empresaService: EmpresaService,
    private analiseService: AnaliseService,
    private location: Location
  ) {}

  formatarData() {
    if (
      this.evento.data_inicio != null ||
      this.evento.data_inicio != undefined
    ) {
      this.startDateString = formatDate(
        this.evento.data_inicio.toString(),
        'dd/MM/yyyy',
        'en-US'
      );
    }

    if (
      this.evento.data_conclusao != null ||
      this.evento.data_conclusao != undefined
    ) {
      this.endDateString = formatDate(
        this.evento.data_conclusao.toString(),
        'dd/MM/yyyy',
        'en-US'
      );
    }
  }

  async ngOnInit(): Promise<void> {
    this.keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    this.keyEvento = this.route.snapshot.paramMap.get('keyEvento');
    this.keyProcesso = this.route.snapshot.paramMap.get('keyProcesso');

    await this.empresaService
      .getEmpresa(this.keyEmpresa)
      .subscribe((empresa) => {
        this.empresa = empresa;
      });

    await this.empresaService
      .getProcesso(this.keyEmpresa, this.keyProcesso)
      .subscribe(
        (processo) => {
          this.processo = processo;
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
    await this.analiseService
      .getEvento(this.keyEmpresa, this.keyProcesso, this.keyEvento)
      .subscribe(
        (evento) => {
          this.evento = evento;
          this.evento.recomendacao = this.analiseService.calculoRecomendacao(
            this.evento
          );
          this.textos = this.analiseService.setTextsRelatorio(this.evento);
          this.formatarData();
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
  }

  salvar(): void {
    this.analiseService.setEvento(
      this.keyEmpresa,
      this.evento.key_processo,
      this.evento
    );
    this.backURL();
  }

  backURL(): void {
    this.location.back();
  }
}
