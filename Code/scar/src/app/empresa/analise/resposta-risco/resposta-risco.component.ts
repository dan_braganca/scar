import { formatDate, Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpresaModel } from 'src/app/models/empresa.model';
import { EventoModel } from 'src/app/models/evento.model';
import { ProcessoModel } from 'src/app/models/processo.model';
import { AnaliseService } from 'src/app/services/analise.service';
import { EmpresaService } from 'src/app/services/empresa.service';

@Component({
  selector: 'app-resposta-risco',
  templateUrl: './resposta-risco.component.html',
  styleUrls: ['./resposta-risco.component.scss'],
})
export class RespostaRiscoComponent implements OnInit {
  keyEmpresa = '';
  keyProcesso = '';
  keyEvento = '';

  startDateString: string;
  endDateString: string;

  processo: ProcessoModel;
  empresa: EmpresaModel;
  evento: EventoModel;

  constructor(
    private route: ActivatedRoute,
    private empresaService: EmpresaService,
    private analiseService: AnaliseService,
    private location: Location
  ) {}

  formatarData() {
    if (
      this.evento.data_inicio != null ||
      this.evento.data_inicio != undefined
    ) {
      this.startDateString = formatDate(
        this.evento.data_inicio,
        'dd/MM/yyyy',
        'en-US'
      );
    }

    if (
      this.evento.data_conclusao != null ||
      this.evento.data_conclusao != undefined
    ) {
      this.endDateString = formatDate(
        this.evento.data_conclusao,
        'dd/MM/yyyy',
        'en-US'
      );
    }
  }

  async ngOnInit(): Promise<void> {
    this.keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    this.keyEvento = this.route.snapshot.paramMap.get('keyEvento');
    this.keyProcesso = this.route.snapshot.paramMap.get('keyProcesso');

    await this.empresaService
      .getEmpresa(this.keyEmpresa)
      .subscribe((empresa) => {
        this.empresa = empresa;
      });

    await this.empresaService
      .getProcesso(this.keyEmpresa, this.keyProcesso)
      .subscribe(
        (processo) => {
          this.processo = processo;
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
    await this.analiseService
      .getEvento(this.keyEmpresa, this.keyProcesso, this.keyEvento)
      .subscribe(
        (evento) => {
          this.evento = evento;
          this.formatarData();
          if (this.evento.data_inicio) {
            this.evento.data_inicio = new Date(
              this.evento.data_inicio
            ).toDateString();
          }
          if (this.evento.data_conclusao) {
            this.evento.data_conclusao = new Date(
              this.evento.data_conclusao
            ).toDateString();
          }
        },
        (error) => console.log('Erro em RespostaRisco.component: ', error)
      );
  }

  salvar(): void {
    this.evento.data_inicio = this.evento.data_inicio.toString();
    this.evento.data_conclusao = this.evento.data_conclusao.toString();
    this.analiseService.setEvento(
      this.keyEmpresa,
      this.evento.key_processo,
      this.evento
    );
    this.backURL();
  }

  backURL(): void {
    this.location.back();
  }
}
