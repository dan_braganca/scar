import { EmpresaModel } from './../models/empresa.model';
import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../services/empresa.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit {

  empresas: EmpresaModel[];

  constructor(
    private service: EmpresaService,
    private route: ActivatedRoute
    ) { }

  columnsToDisplay = [
    'razao_social',
    'nome_fantasia',
    'natureza_juridica',
    'atividade',
    'avaliacao'
  ];

  // tslint:disable-next-line: typedef
  getEmpresas() {
    this.service.getEmpresas().subscribe((array) =>{
      this.empresas = array;
    });
    console.log(this.empresas);
  }

  ngOnInit(): void {
    this.getEmpresas();
  }

}
