import { Location } from '@angular/common';
import { ProcessoModel } from 'src/app/models/processo.model';
import { EmpresaService } from './../../../services/empresa.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditProcessoComponent implements OnInit {
  // UTILIZA TWO-WAY DATA-BINDING AO INVÉS DE UTILIZAR FORMGROUP

  keyEmpresa = '';

  processo: ProcessoModel;

  constructor(
    private empresaService: EmpresaService,
    // private analiseService: AnaliseService,
    private route: ActivatedRoute,
    private location: Location,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.keyEmpresa = this.route.snapshot.paramMap.get('keyEmpresa');
    const keyProcesso: string = this.route.snapshot.paramMap.get('keyProcesso');

    if (keyProcesso !== 'novo') {
      this.empresaService.getProcesso(this.keyEmpresa, keyProcesso).subscribe(
        (processo) => {
          this.processo = processo;
        },
        (error) => console.log('Erro em ProcessoEdit.component: ', error)
      );
      // Só quero ver meu objeto.
      console.log(this.processo);
    } else {
      this.processo = new ProcessoModel();
      console.log(this.processo);
    }
  }

  salvar(): void {
    if (this.processo.key == null) {
      this.empresaService.addProcesso(this.keyEmpresa, this.processo);
    } else {
      this.empresaService.setProcesso(this.keyEmpresa, this.processo);
    }
    this.snackBar.open('Processo atualizado com sucesso', 'Ok', {
      verticalPosition: 'top',
      duration: 2000,
    });
    this.location.back();
  }

  async deleteProcesso(): Promise<void> {
    await Swal.fire({
      title: 'Tem certeza de que deseja excluir este processo?',
      text: 'Esta ação é irreversível',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.empresaService.deleteProcesso(this.keyEmpresa, this.processo);
        this.backURL();
      }
    });
  }

  backURL(): void {
    this.location.back();
  }
}
