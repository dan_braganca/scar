import { Router } from '@angular/router';
import { User } from 'firebase';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading: boolean = false;

  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });

  // AuthService é um injetável. Precisa ser setado no construtor usando o encapsulamento private.
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  authenticate(user): void {
    this.authService.login(user.email, user.password).then(
      (res) => {
        this.router.navigate(['empresa']);
      },
      (err) => {
        console.log('Erro de Login.', err);
        this.snackBar.open(
          'Erro de Login. Verifique as informações digitadas',
          'Ok',
          {
            verticalPosition: 'top',
            duration: 2000,
          }
        );
      }
    );
  }
}
