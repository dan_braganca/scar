import { canActivate } from '@angular/fire/auth-guard';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  authenticated$: Observable<User>;

  constructor() { }

  ngOnInit(): void {
  }

}
