import { AuthService } from './../../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  authenticated$: Observable<boolean>;

  user = {
    email: '',
    password: ''
  };

  formRegister: FormGroup = this.fb.group({
    email: ['' , [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
    passwordRepeat: ['', [Validators.required, Validators.minLength(8)]]
  },
  { validator: this.matchingPasswords });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  matchingPasswords(group: FormGroup){
    if (group) {
      const password = group.controls.password.value;
      const passwordRepeat = group.controls.passwordRepeat.value;

      if (password == passwordRepeat){
        return { matching: true };
      } else {
        return { matching: false };
      }
    }
  }

  ngOnInit(): void {

  }

  onSubmit(): void {
    if (this.formRegister.controls.password.value.length >= 8){
      this.authService.register(
        this.formRegister.controls.email.value,
        this.formRegister.controls.password.value).then(res => {
            this.router.navigate(['login']);
            this.snackBar.open('Usuário registrado com sucesso', 'Ok', {
              verticalPosition: 'top',
                duration: 2000,
              });
          }, err => {
            console.log(err);
            this.snackBar.open('Erro: Não foi possível registrar usuário.', 'Ok', {
              verticalPosition: 'top',
              duration: 2000,
            });
          }
        );
    } else {
      this.snackBar.open('Erro: a senha deve ter 8 dígitos ou mais.', 'Ok', {
        verticalPosition: 'top',
        duration: 2000,
      });
    }
  }
}
