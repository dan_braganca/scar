export interface customData {
  nome_processo: string;
  key_processo: string;
  key_evento: string;
  key_empresa: string;
  descricao_evento: string;
  categoria: string;
  impacto: number;
  probabilidade: number;
  risco_inerente: number;
}
