export class MatrizModel {
  id: string;
  data: object;
  height: any;
  width: any;
  constructor(config: object){
    this.id = config['id'];
    this.data = config['data'];
    this.height = config['height'] || 500;
    this.width = config['width'] || 500;
  }
}
