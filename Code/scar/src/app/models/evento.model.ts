import { NumberValueAccessor } from '@angular/forms';

export class EventoModel {
  key: string;
  key_processo: string;
  descricao: string;

  causa?: string;
  efeito?: string;
  categoria?: string;
  /* CATEGORIAS DE RISCO:
  operacional
  estratégico
  orçamentário
  reputação
  integridade
  fiscal
  conformidade
  */

  // variáveis do estudo de risco inerente.
  probabilidade?: number;
  // probabilidade:
  //    1: muito baixa (<= 10%)
  //    2: baixa (10 <= 30%)
  //    3: média (30 <= 50%)
  //    4: alta (50 <= 90%)
  //    5: muito alta ( > 90%)

  impacto?: number;

  risco_inerente?: number; // (impacto * probabilidade) / 2

  //    1: insignificante (<= 10%)
  //    2: pequeno (10 <= 30%)
  //    3: moderado (30 <= 50%)
  //    4: alto (50 <= 90%)
  //    5: crítico ( > 90%)

  // variáveis de impacto do risco inerente.

  // 1: Insignificante
  // 2: Pequeno
  // 3: Moderado
  // 4: Grande
  // 5: Catastrófico

  // Aspectos avaliativos do estudo de impacto do risco inerente.
  // O peso é dado em porcentagem.
  esforco_gestao: number; // Impacto do evento na gestão da organização
  regulacao: number; // Determina o caráter das ações estratégicas de mitigação
  reputacao: number; // Impacto na reputação da organização
  negocios: number; // Negócios e serviços à sociedade: determina o impacto do evento nos negócios
  intervencao: number; // Intervenção hierárquica: determina o nível hierárquico envolvido no impacto
  // tslint:disable: max-line-length
  valor_orcamentario: number; // Controle econômico-financeiro: determina a porcentagem do orçamento da organização envolvido na mitigação do risco
  // Porcentagens do orçamento
  // 1: < 1%
  // 2: = 1 < 3%
  // 3: = 3 < 10%
  // 4: = 10 < 25%
  // 5: >= 25%

  // variáveis do controle.
  controle_atual?: string; // Descrição do controle atualmente utilizado.

  aval_desenho_controle?: number;
  // Avaliação do desenho do controle atual:
  /*
  (1) Não há sistema de Controle;
  (2) Há procedimento de controle para algumas atividades, porém informais;
  (3) Controles não foram planejados formalmente, mas são executados de
      acordo com a experiência dos servidores;
  (4) É desenhado um sistema de controle integrado adequadamente planejado,
      discutido e documentado. O sistema de controle vigente é eficaz, mas não prevê revisões periódicas;
  (5) O sistema de controle é eficaz na gestão de riscos (adequadamente planejado,
      discutido, testado e documentado com correções ou aperfeiçoamentos planejados de forma tempestiva).
  */

  aval_operacao_controle?: number; // ISSO VAI DESCREVER AS AÇÕES DE RISCO QUANDO COLOCADO NA ANÁLISE DE RISCO
  // Avaliação da operação do controle atual
  /*
  (1) Controle não executado;
  (2) Controle parcialmente executado e com deficiências;
  (3) Controle parcialmente executado;
  (4) Controle implantado e executado de maneira periódica e quase sempre uniforme.
      Avaliação dos controles é feita com alguma periodicidade;
  (5) Controle implantado e executado de maneira uniforme pela equipe e na
      frequência desejada. Periodicamente os controles são testados e aperfeiçoados.
  */

  risco_residual?: number; // variáveis do estudo de risco residual. // Variáveis da resposta a risco
  // Índice de risco observado após a implantação dos controles
  /*
  Avaliação será feita com base na avaliação do desenho e da operação dos controles de risco
  (1) Risco residual insignificante
  (2) Risco residual pequeno
  (3) Risco residual moderado
  (4) Risco residual grande
  (5) Risco residual catastrófico
  */

  // variáveis da resposta a risco

  // Níveis de risco para a NR (resposta a risco):
  /*
    risco insignificante: ----
    risco pequeno: Indica que o risco inerente já está dentro da tolerância a risco
      Verificar a possibilidade de retirar controles considerados desnecessários
      Tipo de resposta: Aceitar - Conviver com o evento de risco mantendo práticas e procedimentos existentes
    risco moderado: Indica que o risco residual será reduzido a um nível compatível com a tolerância a riscos
      Reduzir probabilidade ou impacto, ou ambos
      Tipo de resposta: Compartilhar ou transferir - Reduzir a probabilidade ou impacto pela transferência ou compartilhamento de
      uma parte do risco. (seguro, transações de hedge ou terceirização da atividade).
    risco alto: Indica que o risco residual será reduzido a um nível compatível com a tolerância a riscos
      Nem todos os riscos podem ser transferidos. Exemplo: Risco de Imagem, Risco de Reputação
      Tipo de resposta: Reduzir - Adotar medidas para reduzir a probabilidade ou impacto dos riscos, ou ambos
    risco crítico: Indica que nenhuma opção de resposta foi identificada para reduzir a probabilidade e o impacto a nível aceitável
      Custo desproporcional, capacidade limitada diante do risco identificado
      Tipo de resposta: Evitar - Promover ações que evitem/eliminem as causas e/ou efeitos
  */ tipo?: number; // Evitar/ Reduzir/ Compartilhar ou Transferir/ Aceitar
  descricao_nr?: string;
  data_inicio?: string; // Date
  data_conclusao?: string; // Date
  status?: number; // não iniciado/ em andamento/ concluído
  acao_controle?: string;
  recomendacao?: number;
}
