import { EventoModel } from './evento.model';

export class ProcessoModel {
  key: string;
  nome_processo: string;
  descricao_processo: string;

  eventos?: EventoModel[];
}
