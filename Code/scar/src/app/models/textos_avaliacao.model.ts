export class TextosAvaliacaoModel {
  // Textos dos aspectos avaliativos:
  ESFORCO_GESTAO_INSIGNIFICANTE = 'Impacto do evento pode ser absorvido por meio de atividades normais.';
  ESFORCO_GESTAO_PEQUENO = 'Consequências do evento podem ser absorvidas, mas carecem de esforço da gestão para minimizar o impacto.';
  ESFORCO_GESTAO_MODERADO = 'Evento significativo que pode ser gerenciado em circunstâncias normais.';
  ESFORCO_GESTAO_ALTO = 'Evento crítico, mas que com a devida gestão pode ser suportado.';
  ESFORCO_GESTAO_CATASTROFICO = 'Evento com potencial para levar a organização ao colapso.';

  REGULACAO_INSIGNIFICANTE = 'Pouco ou nenhum impacto.';
  REGULACAO_PEQUENO = 'Determina ações de caráter orientativo.';
  REGULACAO_MODERADO = 'Determina ações de caráter corretivo.';
  REGULACAO_ALTO = 'Determina ações de caráter punitivo.';
  REGULACAO_CATASTROFICO = 'Determina interrupção das atividades';

  REPUTACAO_INSIGNIFICANTE = 'Impacto apenas interno ou nenhum impacto.';
  REPUTACAO_PEQUENO = 'Tende a limitar-se às partes envolvidas.';
  REPUTACAO_MODERADO = 'Pode chegar à mídia provocando a exposição por um curto período de tempo.';
  REPUTACAO_ALTO = 'Com destaque na mídia nacional, provocando exposição significativa.';
  REPUTACAO_CATASTROFICO = 'Com destaque na mídia nacional e internacional, provocando exposição total.';

  NEGOCIOS_INSIGNIFICANTE = 'Pouco ou nenhum impacto nas metas.';
  NEGOCIOS_PEQUENO = 'Prejudica o alcance das metas do processo.';
  NEGOCIOS_MODERADO = 'Prejudica o alcance dos objetivos estratégicos.';
  NEGOCIOS_ALTO = 'Prejudica o alcance da missão da organização.';
  NEGOCIOS_CATASTROFICO = 'Tem potencial para interromper a prestação de serviços ou o negócio da organização indefinidamente.';

  INTERVENCAO_INSIGNIFICANTE = 'Sem necessidade de intervenção da gestão, podendo ser tratado no funcionamento normal da atividade.';
  INTERVENCAO_PEQUENO = 'Exige a intervenção da gestão do setor.';
  INTERVENCAO_MODERADO = 'Exige a intervenção da gerência de área.';
  INTERVENCAO_ALTO = 'Exige a intervenção da direção da organização.';
  INTERVENCAO_CATASTROFICO = 'Exige intervenção externa de outras organizações ou de autoridades.';

  VALOR_ORCAMENTARIO_INSIGNIFICANTE = 'O tratamento do evento compromete menos de 1% do orçamento da organização.';
  VALOR_ORCAMENTARIO_PEQUENO = 'O tratamento do evento compromente entre 1% e 3% do orçamento da organização.';
  VALOR_ORCAMENTARIO_MODERADO = 'O tratamento do evento compromente entre 3% e 10% do orçamento da organização.';
  VALOR_ORCAMENTARIO_ALTO = 'O tratamento do evento compromente entre 10% e 25% do orçamento da organização.';
  VALOR_ORCAMENTARIO_CATASTROFICO = 'O tratamento do evento compromente mais de 25% do orçamento da organização.';

  // Textos da probabilidade:
  PROBABILIDADE_MUITOBAIXA = 'Evento pode ocorrer apenas em circunstâncias excepcionais.';
  PROBABILIDADE_BAIXA = 'Evento pode ocorrer em algum momento.';
  PROBABILIDADE_MEDIA = 'Evento deve ocorrer em algum momento.';
  PROBABILIDADE_ALTA = 'Evento provavelmente ocorra na maioria das circunstâncias.';
  PROBABILIDADE_MUITOALTA = 'Evento é esperado na maioria das circunstâncias.';

  // Textos do impacto:
  IMPACTO_MUITOBAIXO = 'O impacto do evento é mínimo sobre as operações da organização.';
  IMPACTO_BAIXO = 'O impacto do evento é pouco relevante sobre as operações da organização.';
  IMPACTO_MEDIO = 'O impacto do evento é significativo sobre as operações da organização';
  IMPACTO_ALTO = 'O impacto compromete consideravelmente as operações da organização.';
  IMPACTO_MUITOALTO = 'O impacto ocasiona colapso às ações de gestão. Toda a viabilidade estratégica da organização pode ser comprometida.';

  // Textos de risco inerente:
  RISCO_INERENTE_INSIGNIFICANTE = 'Evento de risco insignificante. Pode ser facilmente tratado ou apenas ignorado.';
  RISCO_INERENTE_PEQUENO = 'Evento de risco relevante. Requer pouca intervenção em seu tratamento.';
  RISCO_INERENTE_MODERADO = 'Evento de risco significativo. Requer atenção e controle específicos.';
  RISCO_INERENTE_ALTO = 'Evento de risco grave. Requer envolvimento total em sua solução.';
  RISCO_INERENTE_CRITICO = 'Evento de risco catastrófico. Requer que as atividades que o geram sejam imediatamente descontinuadas e revistas.';

  // Textos de risco residual:
  RISCO_RESIDUAL_INSIGNIFICANTE = 'Os controles adotados são totalmente eficazes no gerenciamento do risco deste evento.';
  RISCO_RESIDUAL_PEQUENO = 'Os controles adotados são eficazes, porém precisam ser revistos para que o evento deixe de representar risco.';
  RISCO_RESIDUAL_MODERADO = 'Os controles adotados são parcialmente eficazes e precisam ser atualizados para que se tornem eficazes.';
  RISCO_RESIDUAL_ALTO = 'Os controles adotados são ineficazes e exigem atenção imediata da direção da organização para que o risco seja tratado.';
  RISCO_RESIDUAL_CRITICO = 'Os controles adotados são completamente ineficazes na gestão de risco e o evento representa risco severo para a continuidade das operações.';

  // Textos dos controles de risco:
  DESENHO_CONTROLE_1 = 'Não há controle deste evento.';
  DESENHO_CONTROLE_2 = 'Existem procedimentos de controle, porém são informais.';
  DESENHO_CONTROLE_3 = 'Os procedimentos de controle não foram planejados formalmente, mas são executados de acordo com a experiência dos colaboradores.';
  DESENHO_CONTROLE_4 = 'Existe um planejamento adequado e documentado do controle, mas não há previsão de revisões periódicas deste controle.';
  DESENHO_CONTROLE_5 = 'O sistema de controle é eficaz na gestão de risco, sendo adequadamente planejado, discutido, testado, documentado e com aperfeiçoamentos periódicos.';

  OPERACAO_CONTROLE_1 = 'Não há controle deste evento.';
  OPERACAO_CONTROLE_2 = 'O controle é parcialmente executado e deficiente para a gestão do risco.';
  OPERACAO_CONTROLE_3 = 'O controle é eficiente, porém parcialmente executado.';
  OPERACAO_CONTROLE_4 = 'O controle é implementado e executado de forma periódica.';
  OPERACAO_CONTROLE_5 = 'O controle é implementado e executado de forma periódica e frequente, com atualizações frequentes.';

  // Textos das respostas de risco:
  TIPO_RESPOSTA_1 = 'Evitar';
  TIPO_RESPOSTA_2 = 'Reduzir';
  TIPO_RESPOSTA_3 = 'Compartilhar ou Transferir';
  TIPO_RESPOSTA_4 = 'Aceitar';

  RECOMENDACAO_RESPOSTA_1 = 'Recomenda-se evitar quaisquer atividades que possam desencadear este evento.';
  RECOMENDACAO_RESPOSTA_2 = 'Recomenda-se reduzir a frequência de realização de quaisquer atividades que possam desencadear este evento.';
  RECOMENDACAO_RESPOSTA_3 = 'Recomenda-se compartilhar o risco ou transferir a atividade para outra alçada de responsabilidade.';
  RECOMENDACAO_RESPOSTA_4 = 'Nenhuma atitude para reduzir o risco deste evento é realmente necessária, sendo os controles eficientes ou o risco naturalmente baixo.';

  STATUS_RESPOSTA_1 = 'Não iniciado.';
  STATUS_RESPOSTA_2 = 'Em andamento.';
  STATUS_RESPOSTA_3 = 'Concluído.';
}
