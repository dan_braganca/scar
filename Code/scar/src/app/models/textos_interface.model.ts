export class TextosInterfaceModel {
  def_esforco_gestao: string;
  def_regulacao: string;
  def_reputacao: string;
  def_negocios: string;
  def_intervencao: string;
  def_valor_orcamentario: string;

  def_probabilidade: string;

  def_impacto: string;

  def_risco_inerente: string;

  def_risco_residual: string;

  def_desenho_controle: string;

  def_operacao_controle: string;

  def_tipo_resposta: string;

  def_recomendacao_resposta: string;

  def_status_resposta: string;
}
