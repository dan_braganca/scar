import { ProcessoModel } from './processo.model';

export class EmpresaModel {
  key: string;
  nome_fantasia: string;
  razao_social: string;
  cnpj: string;

  natureza_juridica: string;
  atividade: string;

  processos?: ProcessoModel[];
}
