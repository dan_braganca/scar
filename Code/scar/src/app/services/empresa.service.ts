import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EmpresaModel } from './../models/empresa.model';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ProcessoModel } from '../models/processo.model';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  // Coleção do Firestore das empresas
  private EmpresaCollection: AngularFirestoreCollection<EmpresaModel> = this.FirestoreDb.collection('empresas');

  // Coleção dos processos no Firestore
  private getProcessosCollectionPath(empresa: EmpresaModel): AngularFirestoreCollection<ProcessoModel>{
    return this.FirestoreDb.collection(`empresas/${empresa.key}/processos`);
  }

  // Using: firestoreDb
  constructor(private FirestoreDb: AngularFirestore) {}

  getEmpresas(): Observable<EmpresaModel[]>{
    return this.EmpresaCollection.valueChanges();
  }

  getEmpresa(key: string): Observable<EmpresaModel>{
    const empresa = this.FirestoreDb.doc<EmpresaModel>('empresas/' + key);

    return empresa.snapshotChanges().pipe(
      map((changes) => {
        const data = changes.payload.data();
        const key = changes.payload.id;
        return { key, ...data };
      })
    );
  }

  public getEmpresaData(key_firebase: string): Observable<EmpresaModel> {
    return this.FirestoreDb
      .collection<EmpresaModel>('empresas', (ref) => ref.where('key', '==', key_firebase))
      .valueChanges()
      .pipe(map((r) => (r.length === 0 ? null : r[0])));
  }

  addEmpresa(empresa: EmpresaModel): void{
    const id = this.FirestoreDb.createId();
    empresa.key = id;
    this.EmpresaCollection.doc(id).set(Object.assign({}, empresa));
  }

  // Edit empresa com a coleção de processos
  setEmpresaProcessos(empresa: EmpresaModel, processos: ProcessoModel[]): void{
    console.log(empresa);
    this.EmpresaCollection.doc(empresa.key).set(empresa);

    processos.forEach((processo) => {
      this.getProcessosCollectionPath(empresa).doc(processo.key).update(processo);
    });
  }

  // Edit empresa sem a coleção de processos
  setEmpresa(empresa: EmpresaModel): void{
    console.log(empresa);
    this.EmpresaCollection.doc(empresa.key).set(empresa);
  }

  deleteEmpresa(empresa: EmpresaModel): void{
    const document = this.EmpresaCollection.doc<EmpresaModel>(empresa.key);

    document.delete();
  }

  getProcessosEmpresa(empresa: EmpresaModel): Observable<ProcessoModel[]>{
    return this.getProcessosCollectionPath(empresa).valueChanges();
  }

  getProcesso(keyEmpresa: string, keyProcesso: string): Observable<ProcessoModel>{
    const processo = this.FirestoreDb.doc<ProcessoModel>(`empresas/${keyEmpresa}/processos/${keyProcesso}`);

    return processo.snapshotChanges().pipe(
      map((changes) => {
        const data = changes.payload.data();
        const key = changes.payload.id;
        return { key, ...data };
      })
    );
  }

  addProcesso(keyEmpresa: string, processo: ProcessoModel): void{
    const id = this.FirestoreDb.createId();
    processo.key = id;
    // DIRETO NA COLEÇÃO DOS PROCESSOS
    this.FirestoreDb.collection(`empresas/${keyEmpresa}/processos`)
      .doc(id).set(Object.assign({}, processo));
  }

  setProcesso(keyEmpresa: string, processo: ProcessoModel): void{
    const docProcesso = this.FirestoreDb.doc<ProcessoModel>(`empresas/${keyEmpresa}/processos/${processo.key}`);

    docProcesso.update(processo);
  }

  deleteProcesso(keyEmpresa: string, processo: ProcessoModel): void{
    const docProcesso = this.FirestoreDb.doc<ProcessoModel>(`empresas/${keyEmpresa}/processos/${processo.key}`);
    docProcesso.delete();
  }
}
