import { TextosInterfaceModel } from './../models/textos_interface.model';
import { TextosAvaliacaoModel } from './../models/textos_avaliacao.model';
import { EventoModel } from './../models/evento.model';
import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { EmpresaModel } from '../models/empresa.model';
import { ProcessoModel } from '../models/processo.model';
import { Observable } from 'rxjs';
import { EmpresaService } from './empresa.service';
import { flatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AnaliseService {
  constructor(
    private FirestoreDb: AngularFirestore,
    private empresaService: EmpresaService
  ) {}

  getEventosCollection(
    keyEmpresa: string,
    keyProcesso: string
  ): AngularFirestoreCollection<EventoModel> {
    return this.FirestoreDb.collection(
      `empresas/${keyEmpresa}/processos/${keyProcesso}/eventos`
    );
  }

  getEventosByProcesso(
    keyEmpresa: string,
    keyProcesso: string
  ): Observable<EventoModel[]> {
    return this.getEventosCollection(keyEmpresa, keyProcesso).valueChanges();
  }

  getEvento(
    keyEmpresa: string,
    keyProcesso: string,
    keyEvento: string
  ): Observable<EventoModel> {
    const evento = this.FirestoreDb.doc<EventoModel>(
      `empresas/${keyEmpresa}/processos/${keyProcesso}/eventos/${keyEvento}`
    );

    return evento.snapshotChanges().pipe(
      map((changes) => {
        const data = changes.payload.data();
        const key = changes.payload.id;
        return { key, ...data };
      })
    );
  }

  addEvento(
    keyEmpresa: string,
    keyProcesso: string,
    evento: EventoModel
  ): void {
    const id = this.FirestoreDb.createId();
    evento.key = id;
    evento.impacto = this.calculoImpacto(evento);
    evento.risco_inerente = this.calculoRisco(evento);
    this.getEventosCollection(keyEmpresa, keyProcesso)
      .doc(id)
      .set(Object.assign({}, evento));
  }

  setEvento(
    keyEmpresa: string,
    keyProcesso: string,
    evento: EventoModel
  ): void {
    const docEvento = this.FirestoreDb.doc<EventoModel>(
      `empresas/${keyEmpresa}/processos/${keyProcesso}/eventos/${evento.key}`
    );

    docEvento.update(evento);
  }

  deleteEvento(
    keyEmpresa: string,
    keyProcesso: string,
    evento: EventoModel
  ): void {
    const docEvento = this.FirestoreDb.doc<EventoModel>(
      `empresas/${keyEmpresa}/processos/${keyProcesso}/eventos/${evento.key}`
    );
    docEvento.delete();
  }

  //////////////////////////////////////////////////////////////////////////
  //                                CÁLCULOS                              //
  calculoImpacto(evento: EventoModel): number {
    const impacto: number =
      (evento.esforco_gestao * 0.15 +
        evento.regulacao * 0.17 +
        evento.reputacao * 0.12 +
        evento.negocios * 0.13 +
        evento.valor_orcamentario * 0.25) /
      4;

    if (impacto <= 0.1) {
      return 1;
    } else if (impacto > 0.1 && impacto <= 0.3) {
      return 2;
    } else if (impacto > 0.3 && impacto <= 0.5) {
      return 3;
    } else if (impacto > 0.5 && impacto <= 0.9) {
      return 4;
    } else if (impacto > 0.9) {
      return 5;
    }
  }

  calculoRisco(evento: EventoModel): number {
    return evento.impacto * evento.probabilidade;
  }

  calculoRiscoResidual(evento: EventoModel): number {
    let resul: number = 0;
    let aval_controle: number =
      evento.aval_desenho_controle * evento.aval_operacao_controle;

    resul = evento.risco_inerente - aval_controle;
    if (resul > 20){
      return 5;
    }else if (resul > 14 && resul <= 20){
      return 4;
    } else if (resul > 6 && resul <= 14){
      return 3;
    } else if (resul > 3 && resul <= 6){
      return 2;
    } else if (resul <= 3){
      return 1;
    }
  }

  calculoRecomendacao(evento: EventoModel): number {

    // Status: não iniciado
    if (evento.risco_residual === 1 && evento.status === 1){
      return 3;
    }
    if (evento.risco_residual === 2 && evento.status === 1){
      return 3;
    }
    if (evento.risco_residual === 3 && evento.status === 1){
      return 3;
    }
    if (evento.risco_residual === 4 && evento.status === 1){
      return 2;
    }
    if (evento.risco_residual === 5 && evento.status === 1){
      return 1;
    }

    // Status: em andamento
    if (evento.risco_residual === 1 && evento.status === 2){
      return 4;
    }
    if (evento.risco_residual === 2 && evento.status === 2){
      return 4;
    }
    if (evento.risco_residual === 3 && evento.status === 2){
      return 3;
    }
    if (evento.risco_residual === 4 && evento.status === 2){
      return 2;
    }
    if (evento.risco_residual === 5 && evento.status === 2){
      return 2;
    }

    // Status: concluído
    if (evento.risco_residual === 1 && evento.status === 3){
      return 4;
    }
    if (evento.risco_residual === 2 && evento.status === 3){
      return 3;
    }
    if (evento.risco_residual === 3 && evento.status === 3){
      return 2;
    }
    if (evento.risco_residual === 4 && evento.status === 3){
      return 1;
    }
    if (evento.risco_residual === 5 && evento.status === 3){
      return 1;
    }
  }

  //                                                                      //
  //////////////////////////////////////////////////////////////////////////

  // tslint:disable-next-line: member-ordering
  TextosAvaliacao: TextosAvaliacaoModel = new TextosAvaliacaoModel();
  // tslint:disable-next-line: member-ordering
  textos: TextosInterfaceModel = new TextosInterfaceModel();
  // MÉTODO PARA CONFIGURAR OS TEXTOS DO RELATÓRIO DE AVALIAÇÃO DE RISCO
  setTextsRelatorio(evento: EventoModel): any {
    switch (evento.probabilidade){
      case 1:
        this.textos.def_probabilidade = this.TextosAvaliacao.PROBABILIDADE_MUITOBAIXA;
        break;
      case 2:
        this.textos.def_probabilidade = this.TextosAvaliacao.PROBABILIDADE_BAIXA;
        break;
      case 3:
        this.textos.def_probabilidade = this.TextosAvaliacao.PROBABILIDADE_MEDIA;
        break;
      case 4:
        this.textos.def_probabilidade = this.TextosAvaliacao.PROBABILIDADE_ALTA;
        break;
      case 5:
        this.textos.def_probabilidade = this.TextosAvaliacao.PROBABILIDADE_MUITOALTA;
        break;
      default:
        this.textos.def_probabilidade = 'Probabilidade do evento não foi definida.';
        break;
    }

    if (evento.risco_inerente <= 3){
      this.textos.def_risco_inerente = this.TextosAvaliacao.RISCO_INERENTE_INSIGNIFICANTE;
    } else if (evento.risco_inerente > 3 && evento.risco_inerente <= 6){
      this.textos.def_risco_inerente = this.TextosAvaliacao.RISCO_RESIDUAL_PEQUENO;
    } else if (evento.risco_inerente > 6 && evento.risco_inerente <= 14){
      this.textos.def_risco_inerente = this.TextosAvaliacao.RISCO_INERENTE_MODERADO;
    } else if (evento.risco_inerente > 14 && evento.risco_inerente <= 20){
      this.textos.def_risco_inerente = this.TextosAvaliacao.RISCO_INERENTE_ALTO;
    } else if (evento.risco_inerente > 20){
      this.textos.def_risco_inerente = this.TextosAvaliacao.RISCO_INERENTE_CRITICO;
    } else {
      this.textos.def_risco_inerente = 'Risco inerente do evento não foi definido.';
    }

    switch (evento.impacto) {
      case 1:
        this.textos.def_impacto = this.TextosAvaliacao.IMPACTO_MUITOBAIXO;
        break;
      case 2:
        this.textos.def_impacto = this.TextosAvaliacao.IMPACTO_BAIXO;
        break;
      case 3:
        this.textos.def_impacto = this.TextosAvaliacao.IMPACTO_MEDIO;
        break;
      case 4:
        this.textos.def_impacto = this.TextosAvaliacao.IMPACTO_ALTO;
        break;
      case 5:
        this.textos.def_impacto = this.TextosAvaliacao.IMPACTO_MUITOALTO;
        break;
      default:
        this.textos.def_impacto = 'Impacto não foi definido. Verifique se algum aspecto avaliativo não foi cadastrado.';
        break;
    }

    switch (evento.risco_residual) {
      case 1:
        this.textos.def_risco_residual = this.TextosAvaliacao.RISCO_RESIDUAL_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_risco_residual = this.TextosAvaliacao.RISCO_RESIDUAL_PEQUENO;
        break;
      case 3:
        this.textos.def_risco_residual = this.TextosAvaliacao.RISCO_RESIDUAL_MODERADO;
        break;
      case 4:
        this.textos.def_risco_residual = this.TextosAvaliacao.RISCO_RESIDUAL_ALTO;
        break;
      case 5:
        this.textos.def_risco_residual = this.TextosAvaliacao.RISCO_RESIDUAL_CRITICO;
        break;
      default:
        this.textos.def_risco_residual = 'Risco residual não foi definido. Verifique o cadastro do controle de risco.';
        break;
    }

    switch (evento.esforco_gestao) {
      case 1:
        this.textos.def_esforco_gestao = this.TextosAvaliacao.ESFORCO_GESTAO_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_esforco_gestao = this.TextosAvaliacao.ESFORCO_GESTAO_PEQUENO;
        break;
      case 3:
        this.textos.def_esforco_gestao = this.TextosAvaliacao.ESFORCO_GESTAO_MODERADO;
        break;
      case 4:
        this.textos.def_esforco_gestao = this.TextosAvaliacao.ESFORCO_GESTAO_ALTO;
        break;
      case 5:
        this.textos.def_esforco_gestao = this.TextosAvaliacao.ESFORCO_GESTAO_CATASTROFICO;
        break;
      default:
        this.textos.def_esforco_gestao = 'Impacto sobre esforço da gestão não foi definido.';
        break;
    }

    switch (evento.regulacao) {
      case 1:
        this.textos.def_regulacao = this.TextosAvaliacao.REGULACAO_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_regulacao = this.TextosAvaliacao.REGULACAO_PEQUENO;
        break;
      case 3:
        this.textos.def_regulacao = this.TextosAvaliacao.REGULACAO_MODERADO;
        break;
      case 4:
        this.textos.def_regulacao = this.TextosAvaliacao.REGULACAO_ALTO;
        break;
      case 5:
        this.textos.def_regulacao = this.TextosAvaliacao.REGULACAO_CATASTROFICO;
        break;
      default:
        this.textos.def_regulacao = 'Impacto sobre regulação não foi definido.';
        break;
    }

    switch (evento.reputacao) {
      case 1:
        this.textos.def_reputacao = this.TextosAvaliacao.REPUTACAO_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_reputacao = this.TextosAvaliacao.REPUTACAO_PEQUENO;
        break;
      case 3:
        this.textos.def_reputacao = this.TextosAvaliacao.REPUTACAO_MODERADO;
        break;
      case 4:
        this.textos.def_reputacao = this.TextosAvaliacao.REPUTACAO_ALTO;
        break;
      case 5:
        this.textos.def_reputacao = this.TextosAvaliacao.REPUTACAO_CATASTROFICO;
        break;
      default:
        this.textos.def_reputacao = 'Impacto sobre reputação não foi definido.';
        break;
    }

    switch (evento.negocios) {
      case 1:
        this.textos.def_negocios = this.TextosAvaliacao.NEGOCIOS_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_negocios = this.TextosAvaliacao.NEGOCIOS_PEQUENO;
        break;
      case 3:
        this.textos.def_negocios = this.TextosAvaliacao.NEGOCIOS_MODERADO;
        break;
      case 4:
        this.textos.def_negocios = this.TextosAvaliacao.NEGOCIOS_ALTO;
        break;
      case 5:
        this.textos.def_negocios = this.TextosAvaliacao.NEGOCIOS_CATASTROFICO;
        break;
      default:
        this.textos.def_negocios = 'Impacto sobre negócios e serviços não foi definido.';
        break;
    }

    switch (evento.intervencao) {
      case 1:
        this.textos.def_intervencao = this.TextosAvaliacao.INTERVENCAO_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_intervencao = this.TextosAvaliacao.INTERVENCAO_PEQUENO;
        break;
      case 3:
        this.textos.def_intervencao = this.TextosAvaliacao.INTERVENCAO_MODERADO;
        break;
      case 4:
        this.textos.def_intervencao = this.TextosAvaliacao.INTERVENCAO_ALTO;
        break;
      case 5:
        this.textos.def_intervencao = this.TextosAvaliacao.INTERVENCAO_CATASTROFICO;
        break;
      default:
        this.textos.def_intervencao = 'Nível de intervenção hierárquica não foi definido.';
        break;
    }

    switch (evento.valor_orcamentario) {
      case 1:
        this.textos.def_valor_orcamentario = this.TextosAvaliacao.VALOR_ORCAMENTARIO_INSIGNIFICANTE;
        break;
      case 2:
        this.textos.def_valor_orcamentario = this.TextosAvaliacao.VALOR_ORCAMENTARIO_PEQUENO;
        break;
      case 3:
        this.textos.def_valor_orcamentario = this.TextosAvaliacao.VALOR_ORCAMENTARIO_MODERADO;
        break;
      case 4:
        this.textos.def_valor_orcamentario = this.TextosAvaliacao.VALOR_ORCAMENTARIO_ALTO;
        break;
      case 5:
        this.textos.def_valor_orcamentario = this.TextosAvaliacao.VALOR_ORCAMENTARIO_CATASTROFICO;
        break;
      default:
        this.textos.def_valor_orcamentario = 'Impacto sobre valor orçamentário não foi definido.';
        break;
    }

    switch (evento.aval_desenho_controle) {
      case 1:
        this.textos.def_desenho_controle = this.TextosAvaliacao.DESENHO_CONTROLE_1;
        break;
      case 2:
        this.textos.def_desenho_controle = this.TextosAvaliacao.DESENHO_CONTROLE_2;
        break;
      case 3:
        this.textos.def_desenho_controle = this.TextosAvaliacao.DESENHO_CONTROLE_3;
        break;
      case 4:
        this.textos.def_desenho_controle = this.TextosAvaliacao.DESENHO_CONTROLE_4;
        break;
      case 5:
        this.textos.def_desenho_controle = this.TextosAvaliacao.DESENHO_CONTROLE_5;
        break;
      default:
        this.textos.def_desenho_controle = 'Avaliação de desenho de controle não foi definida.';
        break;
    }

    switch (evento.aval_operacao_controle) {
      case 1:
        this.textos.def_operacao_controle = this.TextosAvaliacao.OPERACAO_CONTROLE_1;
        break;
      case 2:
        this.textos.def_operacao_controle = this.TextosAvaliacao.OPERACAO_CONTROLE_2;
        break;
      case 3:
        this.textos.def_operacao_controle = this.TextosAvaliacao.OPERACAO_CONTROLE_3;
        break;
      case 4:
        this.textos.def_operacao_controle = this.TextosAvaliacao.OPERACAO_CONTROLE_4;
        break;
      case 5:
        this.textos.def_operacao_controle = this.TextosAvaliacao.OPERACAO_CONTROLE_5;
        break;
      default:
        this.textos.def_operacao_controle = 'Avaliação de operação de controle não foi definida.';
        break;
    }

    switch (evento.tipo) {
      case 1:
        this.textos.def_tipo_resposta = this.TextosAvaliacao.TIPO_RESPOSTA_1;
        break;
      case 2:
        this.textos.def_tipo_resposta = this.TextosAvaliacao.TIPO_RESPOSTA_2;
        break;
      case 3:
        this.textos.def_tipo_resposta = this.TextosAvaliacao.TIPO_RESPOSTA_3;
        break;
      case 4:
        this.textos.def_tipo_resposta = this.TextosAvaliacao.TIPO_RESPOSTA_4;
        break;
      default:
        this.textos.def_tipo_resposta = 'Tipo de resposta não foi definido.';
        break;
    }

    switch (evento.recomendacao) {
      case 1:
        this.textos.def_recomendacao_resposta = this.TextosAvaliacao.RECOMENDACAO_RESPOSTA_1;
        break;
      case 2:
        this.textos.def_recomendacao_resposta = this.TextosAvaliacao.RECOMENDACAO_RESPOSTA_2;
        break;
      case 3:
        this.textos.def_recomendacao_resposta = this.TextosAvaliacao.RECOMENDACAO_RESPOSTA_3;
        break;
      case 4:
        this.textos.def_recomendacao_resposta = this.TextosAvaliacao.RECOMENDACAO_RESPOSTA_4;
        break;
      default:
        this.textos.def_recomendacao_resposta = 'Recomendação de ação de resposta não pôde ser definida. Verifique controle e aspectos avaliativos.';
        break;
    }

    switch (evento.status) {
      case 1:
        this.textos.def_status_resposta = this.TextosAvaliacao.STATUS_RESPOSTA_1;
        break;
      case 2:
        this.textos.def_status_resposta = this.TextosAvaliacao.STATUS_RESPOSTA_2;
        break;
      case 3:
        this.textos.def_status_resposta = this.TextosAvaliacao.STATUS_RESPOSTA_3;
        break;
      default:
        this.textos.def_status_resposta = 'Status da resposta ao risco não foi definido.';
        break;
    }

    return this.textos;
  }
}
