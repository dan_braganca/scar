import { EmpresaService } from './services/empresa.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialImportModule } from './material-import.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './admin/login/login.component';
import { RegisterComponent } from './admin/register/register.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { AnaliseService } from './services/analise.service';
import { DetalheComponent } from './empresa/detalhe/detalhe.component';
import { ProcessosComponent } from './empresa/processos/processos.component';
import { EditProcessoComponent } from './empresa/processos/edit/edit.component';
import { AnaliseComponent } from './empresa/analise/analise.component';
import { EditEmpresaComponent } from './empresa/edit/edit.component';
import { EditEventoComponent } from './empresa/analise/edit-evento/edit-evento.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { ControlesComponent } from './empresa/analise/controles/controles.component';
import { RespostaRiscoComponent } from './empresa/analise/resposta-risco/resposta-risco.component';
import { AvaliacaoRiscoComponent } from './empresa/analise/avaliacao-risco/avaliacao-risco.component';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';

// export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    EmpresaComponent,
    DetalheComponent,
    ProcessosComponent,
    EditProcessoComponent,
    AnaliseComponent,
    EditEmpresaComponent,
    EditEventoComponent,
    ControlesComponent,
    RespostaRiscoComponent,
    AvaliacaoRiscoComponent,
    /* MaterialImportModule, */
  ],
  imports: [
    BrowserModule,
    MaterialImportModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgxMaskModule.forRoot(),
    // NgxMaskModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    /* AngularFireAuthGuard, */
    AngularFireAuthModule,
    AdminRoutingModule,
    FormsModule,
    // Plotly,
    AppRoutingModule,
    GoogleChartsModule.forRoot({ version: 'current' }),
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    AngularFireModule,
    AngularFireAuth,
    AuthService,
    AnaliseService,
    AuthGuard,
    AuthService,
    EmpresaService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
